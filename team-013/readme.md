# EDOM - Project Research Topic
## Code Generation - Introduction
A long time ago, code generation referred to the final phase of a compiler. Having digested the source code, the compiler would pump out the "code" for the target machine. At that time, "machine" meant a CPU instruction set - so this was machine operation code being generated. These days, 'code generation' is so well established that Wikipedia has a new meaning for it - i.e. "source code generation" rather than "machine code generation"... and it has all sorts of new terms and ideas swirling around it.    

Code Generation can be used for small portions of code or entire applications. It can be used with widely different programming languages and using different techniques. The reason to use code generation are fundamentally three: **productivity**, **portability**, and **consistency**.

* **Productivity**: With code generation you can write the generator once and it can be reused as many times as you need. Changing the input and invoking it is significantly faster than writing the code manually, therefore code generation permits to save time.   

* **Portability**: Once you have a process to generate code for a certain language or framework you can simply change the generator and target a different language or framework. You can also target multiple platforms at once. For example, with a parser generator you can get a parser in C#, Java and C++.    Another example: you might write a UML diagram and use code generation to create both a skeleton class in C# and the SQL code to create a database for MySQL. So the same abstract description can be used to generate different kinds of artifacts.    

* **Consistency**: With code generation you get always the code you expect. The generated code is designed according to the same principles, the naming rule match, etc. The code always works the way you expect, of course except in the case of bugs in the generator. The quality of the code is consistent. With code written manually instead you can have different developers use different styles and occasionally introduce errors even in the most repetitive code.    

Despite this, as everything in life, code generation is not perfect. When using this approach, some issues may arise regarding **maintenance** and **complexity**.

* **Maintenance**: When you use a code generator tool your code becomes dependent on it. A code generator tool must be maintained. If you created it you have to keep updating it, if you are just using an existing one you have to hope that somebody keep maintaining it or you have to take over yourself. So the advantages of code generation are not free. This is especially risky if you do not have or cannot find the right competencies to work on the generator.   

* **Complexity**: Code generated automatically tend to be more complex than code written by hand. Sometimes it has to do with _glue_ code, needed to link different parts together, or the fact that the generator supports more use cases than the one you need. In this second case the generated code can do more than what you want, but this is not necessarily an advantage. Generated code is also surely less optimized than the one you can write by hand. Sometimes the difference is small and not significant, but if your application need to squeeze every bit of performance code generation might not be optimal for you.   

A simple example of code generation are, for example, the templates that IDE's provide to developers. For example, when you building a Java application and you want to create a class, you rarely create an empty file and write everything from scratch. Most of the times, you end up using templates provided by your IDE to generate parts that are repetitive. You could definitely write that code yourself, you would just waste some time performing a menial task.   


# MPS Basic Notions

#### Abstract Syntax Tree (AST)

MPS differentiates from other language workbenches by avoiding the text form. MPS  programs are always represented by an AST. This allows to avoid defining grammar and building a parser to define languages. Instead defines language in terms of types of AST nodes and rules for their mutual relationships. Almost everything that works with in the MPS editor is an AST-node, belonging to an Abstract Syntax Tree (AST).

![](https://confluence.jetbrains.com/download/attachments/116622708/AST.png?version=1&modificationDate=1454518543000&api=v2)

### Nodes
Nodes form a tree. Each node has a parent node (except for root nodes), child nodes, properties, and references to other nodes. The AST-nodes are organized into models. The nodes that don't have a parent, called root nodes. These are the top-most elements of a language.

### Concepts
Nodes can be very different from one another. Each node stores a reference to its declaration, which its called a concept. A concept sets a "type" of connected nodes. It specifies which children, properties, and references an instance of a node can have. Concept declarations form an inheritance hierarchy. If one concept extends another, it inherits all the properties from its parent.
Since everything in MPS revolves around AST, concept declarations are AST-nodes themselves.

![](https://confluence.jetbrains.com/download/attachments/116622708/concepts.png?version=1&modificationDate=1454518543000&api=v2)

### Language
A language in MPS is a set of concepts with some additional information. The additional information includes details on editors, completion menus, intentions, typesystem, dataflow, etc. associated with the language. This information forms several language aspects, including a language can extend another language. An extending language can use any concepts defined in the extended language as types for its children or references, and its concepts can inherit from any concept of the extended language, being fully reusable components.

### Generator

Generators can transform the code created based of different languages that are stored in models. Generators perform model-to-model conversion on AST models. The target models serve one or more purposes:

- can be converted to text source files and then compiled with standard compilers (Java, C, etc.);
- can be converted to text documents and used as such (configuration, documentation - property files, xml, pdf, html, latex);
- can be directly interpreted;
- can be used for code analysis or formal verification by a third party tool (CBMS, state-machine reachability analysis, etc.);
- can be used for simulation of the real system.

Generators typically rely on a Domain framework - a set of libraries that the generated code calls or inherits from. The framework encodes the stable part of the desired solution, while the variable part is contained in the actual generated code.
Generation in MPS is done in phases - the output of one generator can become the input for another generator in a pipeline. An optional model-to-text conversion phase (TextGen) may follow to generate code in a textual format. This staged approach helps bridge potentially big semantics gaps between the original problem domain and the technical implementation domain. It also encourages re-use of generators.
![](https://confluence.jetbrains.com/download/attachments/116622708/Domain_framework.png?version=1&modificationDate=1509104219000&api=v2)

# Acceleo & MPS
[Eclipse Xtext](http://eclipse.org/Xtext) supports the creation of extremely powerful text editors. It also generates a meta-model that represents the abstract syntax of the grammar as well as a parser that parses sentences of the language and builds an instance of the meta-model. Since it uses Eclipse EMF as the basis for its meta models, it can be used together with any EMF-based model transformation and code generation tool, like Acceleo. 

### Acceleo

>Is a code generator implementing of the OMG's Model-to-text specification. It supports the developer with most of the features that can be expected from a top quality code generator IDE: simple syntax, efficient code generation, advanced tooling, and features on par with the JDT. Acceleo help the developer to handle the lifecycle of its code generators. Thanks to a prototype based approach, you can quickly and easily create your first generator from the source code of an existing prototype, then with all the features of the Acceleo tooling like the refactoring tools you will easily improve your generator to realize a full fledged code generator.

That is a quote from the [Acceleo website](https://www.eclipse.org/acceleo/)  that describes what Acceleo does: implementing the principles of model-driven design. It is basically an Eclipse plugin that gives a tool to create Java code starting from a EMF Model, according to a template that we specify. The EMF model can be defined in different ways: with an UML diagram or a custom DSL.

The image below shows a life cycle of a dsl. We can transform one model into another using ATL if we need and  use acceleo to generate the code that we want. We can see that acceleo is part of the process of model driven design.

![](https://3.bp.blogspot.com/-BCJkQb3Wka4/VzC4Xxz6IgI/AAAAAAAABYc/3SYEKvpsZWYezV2DAMqyzio5imN0kfNBQCLcB/s1600/book2publicationexample.png)

This image ilustrates better the workflow of an Acceleo project.
![](https://3v866b1ejtn42a3whamzcaap-wpengine.netdna-ssl.com/wp-content/uploads/2018/04/acceleo_workflow.png)

- We can use one or more instances of a model as input. 
- All the files generated are represented in templates using brackets and the pass as arguments all the instances that we want.
```java
[template public generateFactoryInterface(anUseCaseModel: UseCaseModel, associations : Set(Association))]
```
- Then we can use OCL operations between brackets to find and iterate all the instances that we want. Also is common to use logical statements like ifs and elses to ilustrates the logic of the code that we want to generate.
```java
[let actors : OrderedSet(Actor) = anUseCaseModel.actors]
[for (a: Actor | anUseCaseModel.actors)]
case [actors->indexOf(a)/]:
```
- After all the templates being created and the input model is specified, we can generate the code. And thats it. Very straightforward to use, we only need to understand how templates are constructed and how to use ocl operations.

### MPS
The workflow of working with MPS is done mostly inside this software itself: manipulate all aspects of multiple languages in there. To generate requires to do two things:

- Translating high level concepts to lower level concepts (essentially a model to model transformation)
- Have a text generator that takes the lower level concepts and map them directly to text

The default case is the templatebased transformation which uses the concrete syntax of the target language to specify model-to-model transformations, as we can see in the image below. Alternatively, one can use the node API to manually construct the target tree. Finally the textgen DSL is available to generate ASCII text (at the end of the transformation chain).

![](https://image.slidesharecdn.com/mpsconcepts-131115013245-phpapp02/95/concepts-of-jetbrains-mps-56-638.jpg?cb=1384479489)

Generator in MPS is a template-based that consist of two main building blocks:
- mapping configurations and templates. Mapping configurations define which
elements are processed with which templates. For the entities language, we need a root mapping rule and reduction rules; 
- root mapping rules can be used to create new top level artifacts from existing top level artifacts (they map fragments to other fragments).

MPS templates work differently from normal text generation templates such as for example Xpand or StringTemplate, since they are actually model-to-model transformations. Developers first write a structurally correct example model using the target language. Then so called macros are used to change the example model to reflect the input from which we generate.
The generation details of model-to-model will be described in the sample topic.

Its also possible to generate code in a low level, using [LaTeX](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes). MPS allows us to use layouts, functions, a complete language to create the perfect text files combined with specific functions dedicated to text generation to append and indent our text. The example below, ilustrates one example of mindmap using Latext to generate the code.

![](https://res.cloudinary.com/practicaldev/image/fetch/s--6lRGHlON--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/mw1420e1302gtj86amog.png)

Manual code is when you don't know the exact implementation of a given feature but you are sure that the final code will have to follow a given pattern.
This means that after generating the code, someone will have to check out the generated code and extend it in order to fulfill all the requirements for your project. Manual code is usefull when we are writing config's files, formating data aor when the behavior is unknown. For example, a requirement that implements CRUD operations, we know that will have at least CREATE, READ, UPDATE and DELETE operations, but we can not go further than that.

### Code generation with MPS (Sample)
Before we introduce the sample, we need to understand how one project in MPS is structured to fully comprehend the example.

| Concept | Explanation |
| ------ | ------ |
| Project | Project is the main organizational unit in MPS. Projects consist of one or more modules, which themselves consist of models. |
| Models |  Programs in MPS are organized into models. Model is the smallest unit for generation/compilation. For example, "BaseLanguage" is the bottom-line language in MPS, which builds on Java and extends it in many ways, uses models so that each model represents a Java package. |
| Modules | Modules organize models into higher level entities. A module typically consists of several models acompanied with meta information describing module's properties and dependencies. MPS distinguishes several types of modules: solutions, languages, devkits, and generators. |
| Solutions | Solutions are the simplest possible kind of module in MPS. It is just a set of models unified under a common name. |
| Languages | Language is a module that is more complex than a solution and represents a reusable language. It consists of several models, each defining a certain aspect of the language: structure, constraints , actions, behaviors , etc. |
| Generators | Generators define possible transformations of a language into something else, possibly into another languages. Generators may depend on other generators. |
| DevKits | DevKits helps to treat a large group of interconnected languages as a single unit, without need to import a list of individual languages to use them. DevKits can extend other DevKits. The extending DevKit will then carry along all the inherited languages as if they were its own ones. |
| Projects |  A project simply wraps modules that you need to group together and work with them as a unit. |

#### Sample Entities

In order to generate the code, it is needed that we start by creating the concepts.

The following code shows the definition of the Entity concept .

![entityconcept](./Documents/Entity_struture.png)

The concept implements the INamedConcept interface to inherit a name field. It also declares a list of relationships on children attributes.

It is also created two more concepts in order to show how to relate concepts on macros.

![relationshipconcept](./Documents/Relationship_struture.png)

![referenceconcept](./Documents/Entity_Reference.png)

To create the model, it is needed to create a editor so we can format how it is going to be created. 
Editors in MPS are based on cells. Cells are the smallest unit relevant for projection.

![entityEditor](./Documents/Entity_Editor.png)

![relationshipEditor](./Documents/Entity_Relationship_Editor.png)

![referenceEditor](./Documents/Editor_Reference.png)

Now it is possible to create the model. The next codes represent the models created.

![model1](./Documents/Exemple_Model1.png)

This model represents the entity Harry, who is son of James and have the age of 10.

![model2](./Documents/Exemple_Model2.png)

This model represents the entity James, who is the father of Harry and husband of Lilly.

Is also implemented the model that represents the entity Lilly, who is the mother of Harry and wife of James.

After that, we are now able to generate code. The first step is to create a template based to entity.

![TemplateBased](./Documents/entity2Class.png)

The template is composed by the mapping configuration and the template.

The '$' or '$$' are macros that makes the relation between the template and the node.

Macro is a special kind of an annotation concept which can be attached to any node in template code. Macro brings dynamic aspect into otherwise static template-based model transformation.

The '$' on map_entity substitute the map_entity by the Entity name.

![map_entity](./Documents/Name_of_Class.png)

The loop macro go through the node relationships in order to create the target objects.

![loop](./Documents/loop_relationships.png)

![nameField](./Documents/generateNameField.png)

After that we just need to add to the root mapping rules.

![main](./Documents/Main.png)

Lastly, we just have do build the project and we can  check the code generated.

![main](./Documents/Exemple1_GeneratedCode.png)

![main](./Documents/Exemple2_GeneratedCode.png)

![main](./Documents/Exemple3_GeneratedCode.png)
