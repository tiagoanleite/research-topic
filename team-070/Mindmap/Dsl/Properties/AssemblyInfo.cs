#region Using directives

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

#endregion

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle(@"")]
[assembly: AssemblyDescription(@"")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(@"Isep")]
[assembly: AssemblyProduct(@"Mindmap")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion(@"1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: ReliabilityContract(Consistency.MayCorruptProcess, Cer.None)]

//
// Make the Dsl project internally visible to the DslPackage assembly
//
[assembly: InternalsVisibleTo(@"Isep.Mindmap.DslPackage, PublicKey=00240000048000009400000006020000002400005253413100040000010001009558C6B76606C4C7396071033D6A8B43500262D28B4D2584659F20609895B9D148901F2A76EDE64DFC2AF1E0D436A5AEEB291F86D23F473108E592312799E83DACC83450D05C9ED9C096C413E6D08A544CB6EAB2FAF4C479BA12CF7FAC972263791E75227498D0BF759B37753B5D1D63145CA542EF2F6F3BB6DB81CBA37B11D0")]