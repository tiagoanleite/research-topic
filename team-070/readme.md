# EDOM - Project Research Topic

## 1. Describe the concern (Modeling with Microsoft DSL SDK); 

Optamos por fazer uma pesquisa sobre a modelação sem recorrer ao uso da framework que o Eclipse nos proporciona e que foi abordado nas aulas. 

Escolhemos fazer modelação com o SDK da Micrososft por ser uma das grandes empresas que operam no mercado informático, elaborando no final uma

pequena comparação com o que é abordado nas aulas de EDOM ao longo do semestre. Percebendo assim o porquê de seguirmos alguns tutoriais e ferramentas

e não apenas limitar a aceitar a decisão dos professores sem fazer a nossa própria investigação.

## 2.Describe the approach Criaçao de uma DSL utilizando o Microsoft Modeling DSL SDK 

Para efetuarmos esta prova de conceito decidimos criar uma DSL para o mindmap usando as ferramentas que a Microsoft disponibiliza para a criação e implementação de DSL’s. 

Para isso recorremos a um tutorial disponibilizado pela Microsoft que adaptamos ao nosso mindmap.

Durante esse mesmo tutorial houve sempre uma discussão entre o grupo de trabalho aravés de uma sala virtual no Skype, para se evidenciarem as pricipais diferenças na

elaboração do projeto exemplo.

## 3.Present a simple case study illustrating how to apply the approach - Criação da DSL para o mindmap;
Para esta pesquisa resolvemos fazer uma DSL para o porjeto do mindmap abordado nas aulas mas recorredno ao uso do SDK da Microsoft

Podemos assim resumir a 3 passos a construção da DSL:

  1. Definir o Domain Model num ficheiro .dsl
  2. Definir os elementos de notação, como por exemplo as conexões
  3. Definir a visualização do Domain Model através da notação dos elementos

No link abixo é possível abrir a solução criada com Visual Studio:
[Mindmap](Mindmap/)

## 4.Compare the approach with the approaches lectured in the EDOM course; 

Em termos de instalação de componentes a Microsoft facilita o processo, pois só é necessário instalar apenas um componente - Modeling, que permite a craiçao de DSL
No caso do eclipse é necessário intalar alguns plugins para obter a mesma solução.

Contudo em termos de usabilidade o Eclipse torna-se mais intuitivo do que o SDK da MS havendo o caso mais prático, a questão das validações, em que
no SDK da MS é necessário o desenvolvedor as programar manualmente num ficheiro especifico enquanto que o Eclipse inclui uma componente de validações que dá a capacidade de assegurar a integridade do Model.

Mindmap Metamodel das duas abordagens:

![Mindmap Metamodel - Eclipse Modeling](Diagrams/Mindmap.PNG)

![Mindmap Metamodel - Microsoft Dsl SDK](Diagrams/MindMapEclipseModeling.PNG)

### Provide a small review of literature references about the approach 
  [Tutorial da Microsoft](https://docs.microsoft.com/en-us/visualstudio/modeling/getting-started-with-domain-specific-languages?view=vs-2017)
  
  [Consulta teorica](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.118.6383&rep=rep1&type=pdf)
  
  [Presentation](Presentation-team-070.pptx)
