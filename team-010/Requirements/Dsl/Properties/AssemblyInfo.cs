#region Using directives

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

#endregion

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle(@"")]
[assembly: AssemblyDescription(@"")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(@"DEI-ISEP")]
[assembly: AssemblyProduct(@"Requirements")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion(@"1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: ReliabilityContract(Consistency.MayCorruptProcess, Cer.None)]

//
// Make the Dsl project internally visible to the DslPackage assembly
//
[assembly: InternalsVisibleTo(@"org.eclipse.Requirements.DslPackage, PublicKey=0024000004800000940000000602000000240000525341310004000001000100D5A20B193B42BAF4E64BE4168DDC913BE9656C96320640632DF5C906EC33C6E05D814E575A8B6EF36586772C98667C13A68AEB8D449D0EF1F1531205140BF4FFBE0EED02B946D756BB8108810C97BB60766E72C16C65C41B3B5FD6E01C362498AA38FCDD53AFC6B8DADF480EACAE3E010B15B3E18496F9D28C9D64098982BCED")]